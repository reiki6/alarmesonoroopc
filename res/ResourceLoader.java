import java.awt.Image;
import java.awt.Toolkit;
import java.io.File;
import java.net.URL;


public class ResourceLoader {

	static ResourceLoader rl = new ResourceLoader();
	
	public static Image getImage(String fileName){
		return Toolkit.getDefaultToolkit().getImage(rl.getClass().getResource("images/"+fileName));
	}
	
	public static File getMusic(String fileName){
		URL resource = rl.getClass().getResource(fileName);
		File f = new File(resource.getFile());
		return f;
	}
	
}
