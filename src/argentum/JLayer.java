import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;

import javazoom.jl.decoder.JavaLayerException;
import javazoom.jl.player.Player;

public class JLayer {

	Argentum argentum;

	@SuppressWarnings("unused")
	public JLayer(Argentum argentum) throws JavaLayerException {

		this.argentum = argentum;
	
		//argentum.lblNewLabel.setText("entrou no jlayer...");		

		InputStream is = argentum.musica;
		

		MP3MusicaIS musica = new MP3MusicaIS();
		musica.tocar(is);
		musica.start();

//		URL defaultSound = getClass().getResource("JCustomOpc.dll");
//		File soundFile = null;
//		try {
//			soundFile = new File(defaultSound.toURI());
//		} catch (URISyntaxException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//			argentum.lblNewLabel.setText(e.getMessage());
//		}
//		System.out.println("defaultSound " + defaultSound); // check the URL!
//
//		argentum.lblNewLabel.setText(defaultSound.toString());

		//
		// File mp3File;// = new File(path);
		// //
		// mp3File = soundFile;
		// //
		// MP3Musica musica = new MP3Musica();
		// musica.tocar(mp3File);
		// musica.start();
	}

	// }

	

	/**
	 * ====================================================================
	 * CLASS INTERNA MP3 MUSICA QUE EXTENDE DE THREAD PARA TRABALHAR
	 * PERFEITAMENTE NA APLICA��O SEM TRAVAMENTO NA EXECU��O
	 * ====================================================================
	 */
	public static class MP3Musica extends Thread {

		// OBJETO PARA O ARQUIVO MP3 A SER TOCADO
		private File mp3;

		// OBJETO PLAYER DA BIBLIOTECA JLAYER QUE TOCA O ARQUIVO MP3
		private Player player;

		/**
		 * CONSTRUTOR RECEBE O OBJETO FILE REFERECIANDO O ARQUIVO MP3 A SER
		 * TOCADO E ATRIBUI AO ATRIBUTO DA CLASS
		 *
		 * @param mp3
		 */
		public void tocar(File mp3) {
			this.mp3 = mp3;
		}

		/**
		 * ===============================================================
		 * ======================================METODO RUN QUE TOCA O MP3
		 * ===============================================================
		 */
		public void run() {
			try {

				FileInputStream fis = new FileInputStream(mp3);
				BufferedInputStream bis = new BufferedInputStream(fis);

				this.player = new Player(bis);
				System.out.println("Tocando Musica!");

				this.player.play();
				System.out.println("Terminado Musica!");

			} catch (Exception e) {
				System.out.println("Problema ao tocar Musica" + mp3);
				e.printStackTrace();				
			}
		}
	}

	public static class MP3MusicaIS extends Thread {

		// OBJETO PARA O ARQUIVO MP3 A SER TOCADO
		private InputStream mp3;

		// OBJETO PLAYER DA BIBLIOTECA JLAYER QUE TOCA O ARQUIVO MP3
		private Player player;

		/**
		 * CONSTRUTOR RECEBE O OBJETO FILE REFERECIANDO O ARQUIVO MP3 A SER
		 * TOCADO E ATRIBUI AO ATRIBUTO DA CLASS
		 *
		 * @param mp3
		 */
		public void tocar(InputStream mp3) {
			this.mp3 = mp3;
		}

		/**
		 * ===============================================================
		 * ======================================METODO RUN QUE TOCA O MP3
		 * ===============================================================
		 */
		public void run() {
			try {

				// FileInputStream fis = new FileInputStream(mp3);

				BufferedInputStream bis = new BufferedInputStream(mp3);

				this.player = new Player(bis);
				System.out.println("Tocando Musica!");

				this.player.play();
				System.out.println("Terminado Musica!");

			} catch (Exception e) {
				System.out.println("Problema ao tocar Musica" + mp3);
				e.printStackTrace();
			}
		}
	}

}