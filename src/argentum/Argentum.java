import java.awt.AWTException;
import java.awt.CheckboxMenuItem;
import java.awt.Dimension;
import java.awt.Image;
import java.awt.MenuItem;
import java.awt.PopupMenu;
import java.awt.SystemTray;
import java.awt.Toolkit;
import java.awt.TrayIcon;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.InputStream;
import java.net.URL;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;

public class Argentum {
	JFrame window;
	JPanel mainPanel;
	public JLabel lblNewLabel;
	private JLabel lblNewLabel_1;

	public InputStream musica = this.getClass().getResourceAsStream(
			"musica.mp3");
	public boolean modoAlto = false;

	public Argentum() {
		buildNotShow();
		//showWindow();
		trayIcon();
		new AlarmeSonoro(this).run();
	}

	public static void main(String[] args) {
		new Argentum();
	}

	public void buildNotShow() {
		readyWindow();
		readyMainPanel();
	}

	public void buildWindowAndShow() {
		readyWindow();
		readyMainPanel();
		showWindow();
	}

	public void trayIcon() {

		// Check the SystemTray support
		if (!SystemTray.isSupported()) {
			// System.out.println("SystemTray is not supported");
			return;
		}
		final PopupMenu popup = new PopupMenu();
		final TrayIcon trayIcon = new TrayIcon(createImage("bulb.gif",
				"tray icon"));

		final SystemTray tray = SystemTray.getSystemTray();

		// Create a popup menu components
		MenuItem aboutItem = new MenuItem("About");

		CheckboxMenuItem cb1 = new CheckboxMenuItem("Modo Alto da Compadecida");

		MenuItem noneItem = new MenuItem("None");
		MenuItem exitItem = new MenuItem("Exit");

		// Add components to popup menu
		popup.add(aboutItem);
		popup.addSeparator();

		popup.add(cb1);

		popup.addSeparator();

		popup.add(exitItem);

		trayIcon.setPopupMenu(popup);

		try {
			tray.add(trayIcon);
		} catch (AWTException e) {
			System.out.println("TrayIcon could not be added.");
			return;
		}

		trayIcon.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JOptionPane.showMessageDialog(null,
						"This dialog box is run from System Tray");
			}
		});

		aboutItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				showWindow();
			}
		});

		cb1.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {
				int cb1Id = e.getStateChange();
				if (cb1Id == ItemEvent.SELECTED) {
					trayIcon.setImageAutoSize(true);
					modoAlto = true;

				} else {
					trayIcon.setImageAutoSize(false);
					modoAlto = false;
				}
			}
		});

		ActionListener listener = new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				MenuItem item = (MenuItem) e.getSource();
				// TrayIcon.MessageType type = null;
				System.out.println(item.getLabel());

				if ("Error".equals(item.getLabel())) {
					// type = TrayIcon.MessageType.ERROR;
					trayIcon.displayMessage("Sun TrayIcon Demo",
							"This is an error message",
							TrayIcon.MessageType.ERROR);

				} else if ("Warning".equals(item.getLabel())) {
					// type = TrayIcon.MessageType.WARNING;
					trayIcon.displayMessage("Sun TrayIcon Demo",
							"This is a warning message",
							TrayIcon.MessageType.WARNING);

				} else if ("Info".equals(item.getLabel())) {
					// type = TrayIcon.MessageType.INFO;
					trayIcon.displayMessage("Sun TrayIcon Demo",
							"This is an info message",
							TrayIcon.MessageType.INFO);

				} else if ("None".equals(item.getLabel())) {
					// type = TrayIcon.MessageType.NONE;
					trayIcon.displayMessage("Sun TrayIcon Demo",
							"This is an ordinary message",
							TrayIcon.MessageType.NONE);
				}
			}
		};

		noneItem.addActionListener(listener);

		exitItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				tray.remove(trayIcon);
				System.exit(0);
			}
		});

	}

	private void readyWindow() {
		window = new JFrame("Alarme Sonoro Frevow");
		// utiliza o visual do sistema operacional
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
			SwingUtilities.updateComponentTreeUI(window);
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null,
					"Erro no look and fell " + e.getMessage());
		}

		window.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
		window.addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent evt) {
				modoAlto = true;				
			}
		});

	}

	private void readyMainPanel() {
		mainPanel = new JPanel();
		window.getContentPane().add(mainPanel);

		lblNewLabel = new JLabel("New label");
		mainPanel.add(lblNewLabel);

		lblNewLabel_1 = new JLabel(
				"Desenvolvido por: Rodrigo Venancio Barbosa para Unilever Igarassu. Favor, Nao fechar esta tela.");
		mainPanel.add(lblNewLabel_1);
	}

	public void showWindow() {
		window.pack();
		window.setSize(500, 400);

		// Open in Center of Screen
		Dimension dim = window.getSize();
		int width = dim.width;
		int height = dim.height;
		Dimension screen = Toolkit.getDefaultToolkit().getScreenSize();
		int x = (screen.width - width) / 2;
		int y = (screen.height - height) / 2;
		window.setBounds(x, y, width, height);

		window.setVisible(true);
	}

	// Obtain the image URL
	protected static Image createImage(String path, String description) {
		URL imageURL = TrayIconDemo.class.getResource(path);

		if (imageURL == null) {
			System.err.println("Resource not found: " + path);
			return null;
		} else {
			return (new ImageIcon(imageURL, description)).getImage();
		}
	}

}