

import javafish.clients.opc.JOpc;
import javafish.clients.opc.component.OpcGroup;
import javafish.clients.opc.component.OpcItem;
import javafish.clients.opc.exception.ComponentNotFoundException;
import javafish.clients.opc.exception.ConnectivityException;
import javafish.clients.opc.exception.SynchReadException;
import javafish.clients.opc.exception.UnableAddGroupException;
import javafish.clients.opc.exception.UnableAddItemException;
import javafish.clients.opc.variant.Variant;



public class TesteOPC {
  public static void main(String[] args) throws InterruptedException {
    TesteOPC test = new TesteOPC();
    
    JOpc.coInitialize();
    
    JOpc jopc = new JOpc("localhost", "RSLinx OPC Server", "JOPC1");
    
    OpcItem item1 = new OpcItem("[TesteOPC]TesteString2", true, "");
    //OpcItem item2 = new OpcItem("[TesteOPC]sTeste", true, "");
    //OpcItem item1 = new OpcItem("Random.String", true, "");
    
    OpcGroup group = new OpcGroup("group1", true, 500, 0.0f);
    
    group.addItem(item1);
    //group.addItem(item2);
    jopc.addGroup(group);
    
    try {
      jopc.connect();
      System.out.println("JOPC client is connected...");
    }
    catch (ConnectivityException e2) {
      e2.printStackTrace();
    }
    
    try {
      jopc.registerGroups();
      System.out.println("OPCGroup are registered...");
    }
    catch (UnableAddGroupException e2) {
      e2.printStackTrace();
    }
    catch (UnableAddItemException e2) {
      e2.printStackTrace();
    }
    
    synchronized(test) {
      test.wait(50);
    }
    
    // Synchronous reading of item
    int cycles = 2;
    int acycle = 0;
    while (acycle++ < cycles) {
      synchronized(test) {
        test.wait(1000);
      }
      
      try {
        OpcItem responseItem = jopc.synchReadItem(group, item1);
        System.out.println("response item: " + responseItem);
        System.out.println("response item value: " + responseItem.getValue());
        System.out.println(Variant.getVariantName(responseItem.getDataType()) + " -- : " + responseItem.getValue());
        //responseItem = jopc.synchReadItem(group, item2);
        //System.out.println("response item2 value: " + responseItem.getValue());
        OpcGroup readGroup = jopc.synchReadGroup(group);
        OpcItem[] itens=  readGroup.getItemsAsArray();
        for(OpcItem o: itens){
        	System.out.println("Item: "+ o.getItemName() + " - "+ o.getValue());
        }
      }
      catch (ComponentNotFoundException e1) {
        e1.printStackTrace();
      }
      catch (SynchReadException e) {
        e.printStackTrace();
      }
    }
    
    JOpc.coUninitialize();
  }
}
