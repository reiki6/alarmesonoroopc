import javafish.clients.opc.JOpc;
import javafish.clients.opc.component.OpcGroup;
import javafish.clients.opc.component.OpcItem;
import javazoom.jl.decoder.JavaLayerException;

public class AlarmeSonoro extends Thread {

	boolean tocandoMusica = false;
	Argentum argentum;
	

	public AlarmeSonoro(Argentum argentum) {
		this.argentum = argentum;
	}
	
	public void atualizaMusica(){
		
	}

	@Override
	public void run() {
		while (true) {

			try {
				if (!tocandoMusica) {
					argentum.lblNewLabel.setText("verifica alarme...");
					System.out.println("Verifica Alarme");
					verificaVariavel();					
					sleep(2000);
					
				} else {
					System.out.println("dorme");
					if(argentum.modoAlto){
						sleep(30000);
					}else{
						sleep(5000);
					}
					tocandoMusica = false;
				}
			} catch (Exception e) {
				argentum.lblNewLabel.setText("err2..." + e.getMessage());
			}
		}
	}

	public void verificaVariavel() {

		try {
			argentum.lblNewLabel.setText("Verificar Alarme...");
			JOpc.coInitialize();			
			JOpc jopc = new JOpc("192.168.150.3", "RSLinx OPC Server", "JOPC1");

			OpcItem item1 = new OpcItem("[Frevow2]AlarmeSonoroDINT", true, "");
			OpcGroup group = new OpcGroup("group1", true, 500, 0.0f);
			group.addItem(item1);
			jopc.addGroup(group);

			jopc.connect();
			System.out.println("JOPC client is connected...");
			
			jopc.registerGroups();
			OpcItem responseItem = jopc.synchReadItem(group, item1);

			int respp = responseItem.getValue().getInteger();
			System.out.println(respp);			
			JOpc.coUninitialize();
			
			if (respp != 0) {
				tocandoMusica = true;
				argentum.lblNewLabel.setText("toca a musica...");
				if(argentum.modoAlto){
					argentum.musica = this.getClass().getResourceAsStream("musica2.mp3");
				}else{
					argentum.musica = this.getClass().getResourceAsStream("musica.mp3");
				}								
				tocaMusica();
			} else {
				argentum.lblNewLabel.setText("Sem Alarmes...");
			}

		} catch (Exception e) {
			argentum.lblNewLabel.setText("err1..." + e.getMessage());
		}
	}

	public void tocaMusica() throws JavaLayerException {
		new JLayer(argentum);
	}
}
